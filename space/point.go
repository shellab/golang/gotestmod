package space

import (
	"fmt"
)

type IPoint interface { // public
	X() int
	Y() int
	SetX(int)
	SetY(int)
}

type point struct { // private, invisable from the other package
	x int
	y int
}

func NewPoint(x int, y int) IPoint { // construct, return interface (interface is a pointer)
	p := &point{
		x: x,
		y: y,
	}
	return p
}

func (p *point) String() string { // implement debug
	return fmt.Sprintf("x: %d y: %d", p.x, p.y)
}

func (p *point) X() int { // implement get property
	return p.x
}

func (p *point) Y() int { // implement get property
	return p.y
}

func (p *point) SetX(x int) { // implement set property
	p.x = x
}

func (p *point) SetY(y int) { // implement set property
	p.y = y
}
