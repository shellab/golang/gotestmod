package space

import (
	"fmt"
)

// Point3D

type IPoint3D interface {
	IPoint // extends interface
	Z() int
	SetZ(int)
	Show()
}

type Point3D struct { // public, visable from the other package
	point// extends from the point struct
	z int
}

func NewPoint3D(x int, y int, z int) *Point3D { // return struct pointer
	p := &Point3D{}
	p.SetX(x)
	p.SetY(y)
	p.SetZ(z)
	// or
	/*
	p := &Point3D{
		point: point{
			x: x,
			y: y,
		},
		z: z,
	}
	*/
	// or
	/*
	p := &Point3D{}
	p.x = x
	p.y = y
	p.z = z
	*/
	return p
}

func (p *Point3D) Z() int {
	return p.z
}

func (p *Point3D) SetZ(z int) {
	p.z = z
}

func (p *Point3D) Show() { // override show method
	fmt.Println("X:", p.point.X(), "Y:", p.point.Y(), "Z:", p.Z())
}

func (p *Point3D) String() string {
	return fmt.Sprintf("x: %d y: %d z: %d", p.x, p.y, p.z)
}
